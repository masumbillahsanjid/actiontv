import 'package:actiontv/radio_page.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/material.dart';
import 'package:chewie/chewie.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:video_player/video_player.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:url_launcher/url_launcher.dart';
import 'news_page.dart';

var data;
var TVURL;
var Second;
bool isloading = true;

class MainView extends StatefulWidget {
  @override
  _MainViewState createState() => _MainViewState();
}

class _MainViewState extends State<MainView> {
  bool _isloading = true;
  VideoPlayerController _videoPlayerController1;
  VideoPlayerController _videoPlayerController2;
  ChewieController _chewieController;
  ChewieController _chewieController2;

  _launchURL() async {
    const url = 'https://fb.com';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  void dispose() {
    super.dispose();
    _videoPlayerController1.dispose();
    _videoPlayerController2.dispose();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getslider();
    getS();
    _videoPlayerController1 = VideoPlayerController.network('fsf');
    _chewieController = ChewieController(
        videoPlayerController: _videoPlayerController1,
        autoPlay: true,
        aspectRatio: 3 / 2,
        looping: true,
        isLive: true);
  }

  get(url) async {
    _videoPlayerController2 = VideoPlayerController.network('$url');
    _chewieController2 = ChewieController(
      videoPlayerController: _videoPlayerController2,
      aspectRatio: 3 / 2,
      looping: true,
      autoPlay: true,
      isLive: true,
    );
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        Container(
          height: MediaQuery.of(context).orientation == Orientation.landscape
              ? (MediaQuery.of(context).size.height *
                  0.12) // width = 25% of the screen
              : (MediaQuery.of(context).size.height * 0.25),
          child: Carousel(
            radius: Radius.circular(18),
            dotSize: 02,
            indicatorBgPadding: 2.0,
            autoplay: true,
            boxFit: BoxFit.fitWidth,
            images: [
              TVURL == null
                  ? SpinKitFadingCube(
                      color: Colors.red[900],
                      size: 50.0,
                    )
                  : Container(
                      height: 150,
                      width: 150,
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              fit: BoxFit.cover,
                              image: NetworkImage("${TVURL[0]['img']}"))),
                    ),
              TVURL == null
                  ? SpinKitFadingCube(
                      color: Colors.red[900],
                      size: 50.0,
                    )
                  : Container(
                      height: 150,
                      width: 150,
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              fit: BoxFit.cover,
                              image: NetworkImage("${TVURL[1]['img']}"))),
                    ),
              TVURL == null
                  ? SpinKitFadingCube(
                      color: Colors.red[900],
                      size: 50.0,
                    )
                  : Container(
                      height: 150,
                      width: 150,
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              fit: BoxFit.cover,
                              image: NetworkImage("${TVURL[2]['img']}"))),
                    ),
              TVURL == null
                  ? SpinKitFadingCube(
                      color: Colors.red[900],
                      size: 50.0,
                    )
                  : Container(
                      height: 150,
                      width: 150,
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              fit: BoxFit.cover,
                              image: NetworkImage("${TVURL[3]['img']}"))),
                    ),
              TVURL == null
                  ? SpinKitFadingCube(
                      color: Colors.red[900],
                      size: 50.0,
                    )
                  : Container(
                      height: 150,
                      width: 150,
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              fit: BoxFit.cover,
                              image: NetworkImage("${TVURL[4]['img']}"))),
                    ),
            ],
          ),
        ),
        Container(
          height: 38,
          decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  spreadRadius: 1.5,
                  blurRadius: 5.5,
                  offset: Offset(0, 0.5),
                  color: Colors.grey[500],
                )
              ],
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(15),
                  bottomRight: Radius.circular(15))),
          child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                FlatButton(
                  onPressed: () {},
                  child: Text(
                    "LIVE",
                    style: TextStyle(color: Colors.black),
                  ),
                ),
                FlatButton(
                  onPressed: () {
                    _videoPlayerController2.pause();
                    _videoPlayerController1.pause();
                    Route route =
                        MaterialPageRoute(builder: (context) => NewsPage());
                    Navigator.push(context, route);
                  },
                  child: Text(
                    "Academic Info",
                    style: TextStyle(color: Colors.black),
                  ),
                ),
                FlatButton(
                  onPressed: () {
                    _videoPlayerController2.pause();
                    _videoPlayerController1.pause();
                    Route route =
                        MaterialPageRoute(builder: (context) => RadioPage());
                    Navigator.push(context, route);
                  },
                  child: Text(
                    "Notice Board",
                    style: TextStyle(color: Colors.black),
                  ),
                ),
              ]),
        ),
        SizedBox(
          height: 12,
        ),
        _isloading
            ? SizedBox(
                height:
                    MediaQuery.of(context).orientation == Orientation.landscape
                        ? (MediaQuery.of(context).size.height *
                            0.12) // width = 25% of the screen

                        : (MediaQuery.of(context).size.height * 0.35),
                width: double.infinity,
                child: AspectRatio(
                    aspectRatio: 6 / 5,
                    child: Container(
                      child: Chewie(
                        controller: _chewieController,
                      ),
                    )),
              )
            : SizedBox(
                height:
                    MediaQuery.of(context).orientation == Orientation.landscape
                        ? (MediaQuery.of(context).size.height *
                            0.12) // width = 25% of the screen

                        : (MediaQuery.of(context).size.height * 0.40),
                width: double.infinity,
                child: AspectRatio(
                    aspectRatio: 6 / 5,
                    child: Container(
                      child: Chewie(
                        controller: _chewieController2,
                      ),
                    )),
              ),
        //Action tv live row

        SizedBox(
          height: 5,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Image.asset(
              "assets/live.png",
              height: 40,
              width: 80,
            ),
            Text(
              "Boutv TV",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.indigo,
                  fontSize: 18),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 8.0),
              child: Text(
                "LIVE",
                style: TextStyle(
                    fontWeight: FontWeight.bold, color: Colors.redAccent),
              ),
            ),
          ],
        )
      ],
    );
  }



getslider()async{
    var url ="http://cdn.boutv.live/sdvcsdcdcsdfder43543wrfwefcsfsdvcsdcdcsdfder43543wrfwefcsf/slider";
    http.Response response=await http.get(url);

    if(response.statusCode==200){
      var jsondata=json.decode(response.body);
      print(jsondata['results']);
      setState(() {
        TVURL = jsondata['results'];
      });
    }
}
  getS() async {
    var url =
        "http://cdn.boutv.live/sdvcsdcdcsdfder43543wrfwefcsfsdvcsdcdcsdfder43543wrfwefcsf/livetvfedd";
    http.Response response = await http.get(url);
    var jsondata;
    if (response.statusCode == 200) {
      jsondata = json.decode(response.body);
      setState(() {
        Second = jsondata['results'][0]["live_tv_url"];
        _isloading = false;

        get(jsondata['results'][0]["live_tv_url"]);
      });
      print(jsondata['results'][0]["live_tv_url"]);
    } else {
      setState(() {
        _isloading = false;
      });
    }
  }
}
