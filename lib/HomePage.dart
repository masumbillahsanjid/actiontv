import 'package:actiontv/more.dart';
import 'package:actiontv/url.dart';
import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'about.dart';
import 'mainView.dart';
import 'package:chewie/chewie.dart';

VideoPlayerController _videoPlayerController1;
VideoPlayerController _videoPlayerController2;
ChewieController _chewieController;
ChewieController _chewieController2;

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}
class _HomePageState extends State<HomePage> {
  int currentindex = 0;
  var isloading = true;
  int _current = 0;
  Widget callpage(int _current) {
    switch (_current) {
      case 0:
        return MainView();
      case 1:
        return about();
      case 2:
        return url();
      case 3:
        return More();
    }
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(opacity: 0.0),
        flexibleSpace: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,
              colors: <Color>[
                Color.fromRGBO(47, 176, 146, 1.0),
                Color.fromRGBO(48, 177, 147, 1.0)
              ],
            ),
          ),
        ),
        centerTitle: true,
        title: Image.asset(
          "assets/logo4.png",
height: 35,
        ),
      ),
      body: callpage(_current),
      bottomNavigationBar: BottomNavigationBar(
          selectedItemColor: Color.fromRGBO(47, 176, 146, 1.0),
          unselectedItemColor: Colors.blue,
          onTap: (value) {
            setState(() {
              _current = value;
            });
          },
          currentIndex: _current,
          items: [
            BottomNavigationBarItem(
                title: Text("Home"), icon: Icon(FontAwesomeIcons.home)),
            BottomNavigationBarItem(
                title: Text("About"), icon: Icon(FontAwesomeIcons.infoCircle)),
            BottomNavigationBarItem(
                title: Text("Facebook"), icon: Icon(FontAwesomeIcons.facebook)),
            BottomNavigationBarItem(
                title: Text("More"), icon: Icon(FontAwesomeIcons.bars)),
          ]),
    );
  }

}
