import 'dart:async';
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
class GOtoRA extends StatefulWidget {
  @override
  _GOtoRAState createState() => _GOtoRAState();
}
class _GOtoRAState extends State<GOtoRA> {
  final Completer<WebViewController> _completer =
  Completer<WebViewController>();
  bool _isLoadingPage;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _isLoadingPage = true;
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:SafeArea(child:  Stack(
        children: <Widget>[
          WebView(
            initialUrl: 'https://actionfmradio.com',
            javascriptMode: JavascriptMode.unrestricted,
            onWebViewCreated: (WebViewController webViewController) {
              _completer.complete(webViewController);
            },
            onPageFinished: (finish) {
              setState(() {
                _isLoadingPage = false;
              });
            },
          ),
          _isLoadingPage
              ? Container(
            alignment: FractionalOffset.center,
            child: SpinKitDoubleBounce(
              color: Colors.blue,
              size: 50.0,
            ),
          )
              : Container(),
        ],
      ),),
      );
  }

}