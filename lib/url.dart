import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:http/http.dart'as http;
import 'dart:convert';
class url extends StatefulWidget {
  @override
  _urlState createState() => _urlState();
}

class _urlState extends State<url> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getData();
  }
  _launchURL(turl) async {
    var url = '$turl';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold();
  }

  getData() async {
    var url = "https://newsaction.tv/Mobile_Api/ApiAppsData.json";
    http.Response response = await http.get(url);
    var jsondata;
    if (response.statusCode == 200) {
      jsondata = json.decode(response.body);

      if (jsondata['status'] = true) {
        _launchURL(jsondata['Facebook_Link']);
      }
    }
  }
}
