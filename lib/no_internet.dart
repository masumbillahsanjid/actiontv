import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:flutter/material.dart';

class NoInternetPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.centerLeft,
          end: Alignment.centerRight,
          colors: <Color>[
            Color.fromRGBO(47, 176, 146, 1.0),
            Color.fromRGBO(48, 177, 147, 1.0)
          ],
        ),
      ),
      child: Center(
        child: SizedBox(
          child: ScaleAnimatedTextKit(
              onTap: () {
                print("Tap Event");
              },
              text: [
                "No",
                "Internet",
                "Connection",
              ],
              textStyle: TextStyle(
                fontSize: 50.0,
                fontFamily: "Canterbury",
                color: Colors.white,
              ),
              textAlign: TextAlign.start,
              alignment: AlignmentDirectional.topStart // or Alignment.topLeft
          ),
        ),
      ),
    );
  }
}
